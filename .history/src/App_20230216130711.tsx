import React, { useState } from "react";
import { InputComponent } from "./components/InputComponent";
import { ITask } from "./interfaces";
import ButtonComponent from "./components/ButtonComponent";
import TaskComponent from "./components/TaskComponent";
import TaskWrapper from "./components/TaskWrapper";

function App() {
  const [task, setTask] = useState<string>("");
  const [desc, setDesc] = useState<string>("");
  const [date, setDate] = useState<string>("");

  const TASK: Array<ITask> = [];

  const handleSubmit = () => {
    const values = {
      title: task,
      description: desc,
      date: date,
    };
    TASK.push(values);
    console.log(TASK.length);
  };

  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 lg:w-1/3 w-1/2">
        <div className="flex-col space-y-2 ">
          <InputComponent
            type={"text"}
            placeholder={"Entrez une tache"}
            change={task}
            setChange={setTask}
          />
          <InputComponent
            type={"text"}
            placeholder={"Entrez la description"}
            change={desc}
            setChange={setDesc}
          />
          <InputComponent
            type={"date"}
            placeholder={"Choisir la date"}
            change={date}
            setChange={setDate}
          />
          <ButtonComponent text="Ajouter" handleSubmit={handleSubmit} />
          <TaskWrapper>
            {TASK.map((task) => {
              console.log(task);
            })}
          </TaskWrapper>
        </div>
      </div>
    </div>
  );
}

export default App;
