import React, { useState } from "react";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 w-1/3">
        <div className="flex-col space-y-2 ">
          <input
            type="text"
            className="px-3 text-slate-700 w-full py-2 outline-none bg-slate-50 border-2 border-slate-400 rounded-sm "
            placeholder="Entrez une tache"
          />
          <input
            type="text"
            className="px-3 w-full text-slate-700 py-2 outline-none bg-slate-50 border-2 border-slate-400 rounded-sm "
            placeholder="Entrez la description"
          />
          <input
            type="date"
            className="px-3 w-full text-slate-700 py-2 outline-none bg-slate-50 border-2 border-slate-400 rounded-sm "
            placeholder="Entrez la description"
          />
          <button className="px-3 py-2 my-2 bg-slate-900 text-center rouded-sm font-bold uppercase">
            Ajouter
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
