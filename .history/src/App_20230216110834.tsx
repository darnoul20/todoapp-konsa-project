import React, { useState } from "react";
import { InputComponent } from "./components/InputComponent";

function App() {
  const [task, setTask] = useState<string>("");
  const [desc, etDesc] = useState<string>("");
  const [date, setDesc] = useState<string>("");

  console.log("task", task);
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 lg:w-1/3 w-1/2">
        <div className="flex-col space-y-2 ">
          <InputComponent
            type={"text"}
            placeholder={"Entrez une tache"}
            change={task}
            setChange={(e: any) => setTask(e.target.value)}
          />
          <InputComponent
            type={"text"}
            placeholder={"Entrez la description"}
            change={undefined}
            setChange={undefined}
          />
          <InputComponent
            type={"date"}
            placeholder={"Choisir la date"}
            change={undefined}
            setChange={undefined}
          />
          <button className="px-3 py-2 my-2 w-full text-slate-50 text-md bg-slate-900 text-center rounded-sm font-bold uppercase">
            Ajouter
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
