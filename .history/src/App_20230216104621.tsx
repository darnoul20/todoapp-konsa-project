import React, { useState } from "react";
import { InputComponent } from "./components/InputComponent";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 w-1/3">
        <div className="flex-col space-y-2 ">
          <InputComponent type={"text"} placeholder={"Entrez une tache"} />
          <InputComponent type={"text"} placeholder={"Entrez la description"} />
          <InputComponent type={"date"} placeholder={"Choisir la date"} />
          <button className="px-3 py-2 my-2 w-full text-slate-50 text-md bg-slate-900 text-center rounded-sm font-bold uppercase">
            Ajouter
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
