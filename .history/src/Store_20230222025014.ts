import { IPostsData } from "./interfaces";

export default class Store {
  restClient: any;
  constructor() {
    this.restClient = () => fetch("https://jsonplaceholder.typicode.com/posts");
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }

  fetchOneData(data: IPostsData) {
    return this.restClient;
  }
}
