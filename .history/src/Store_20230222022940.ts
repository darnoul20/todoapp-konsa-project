export default class Store {
  restClient: Promise<Response>;
  constructor() {
    const options = {
      method: "GET",
      headers: {
        "X-RapidAPI-Key": "cba2e5cc12msh988465028ea3545p1f4d01jsn6855c182480f",
        "X-RapidAPI-Host": "api-football-v1.p.rapidapi.com",
      },
    };
    this.restClient = fetch(
      "https://api-football-v1.p.rapidapi.com/v3/timezone",
      options
    );
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }
}
