import React, { useState } from "react";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-10 shadow-md flex flex-col rounded-sm bg-slate-200">
        <input
          type="text"
          className="p-3 outline-none bg-slate-400 border-2 border-slate-600 rounded-sm "
          placeholder="Entrez une tache"
        />
      </div>
    </div>
  );
}

export default App;
