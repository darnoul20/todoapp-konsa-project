export interface ITask {
  title: string;
  description: string;
  date: string;
  isImportant: boolean;
}

export interface IInput {
  type: string;
  placeholder: string;
  change: string | undefined;
  setChange: any;
}

export interface IButton {
  text: string;
  handleSubmit: any;
}

export interface ITaskComponent {
  title: string;
  description: string;
  date: string;
  isImportant: boolean;
}

export interface IPostsData {
  userId: number;
  id: number;
  title: string;
  body: string;
}
