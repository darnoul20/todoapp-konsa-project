export interface ITask {
    title: string;
    description: string;
    date: string;
}

export interface IInput {
    type: string;
    placeholder: string;
    change: string | undefined,
    setChange: any
}

export interface IButton {
    text: string;
    handleSubmit: any
}

export interface ITaskComponent {
    title: string;
    description: string;
    date: string;
}