export interface ITask {
    title: string;
    description: string;
    important: boolean;
    date: Date;
}