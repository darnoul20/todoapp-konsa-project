export interface ITask {
    title: string;
    description: string;
    important: boolean;
    date: string;
}

export interface IInput {
    type: string;
    placeholder: string;
    change: string | undefined,
    setChange: any
}