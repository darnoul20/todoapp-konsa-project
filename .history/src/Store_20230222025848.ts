import { IPostsData } from "./interfaces";

export default class Store {
  allDatas: Promise<Response>;
  constructor() {
    const restClient = (path: sting) => {
      return fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "get",
      });
    };

    this.allDatas = restClient();
  }

  fetchDatas() {
    return this.allDatas.then((response) => response.json());
  }

  fetchOneData(data: IPostsData) {
    return this.allDatas;
  }
}
