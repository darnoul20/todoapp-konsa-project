export default class Store {
  restClient: Promise<Response>;
  constructor() {
    this.restClient = fetch(
      "https://api-football-v1.p.rapidapi.com/v3/timezone",
      options
    );
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }
}
