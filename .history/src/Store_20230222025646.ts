import { IPostsData } from "./interfaces";

export default class Store {
  constructor() {
    const restClient = () => {
      return fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "get",
      });
    };
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }

  fetchOneData(data: IPostsData) {
    return this.restClient;
  }
}
