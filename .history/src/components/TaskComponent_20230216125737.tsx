import React from "react";
import { ITaskComponent } from "../interfaces";
import { AiFillDelete } from "react-icons/ai";

const TaskComponent: React.FC<ITaskComponent> = ({
  title,
  description,
  date,
}) => {
  return (
    <div className="flex p-2 border-l-4 border-green-600 flex-row justify-between w-full">
      <span className="flex-col space-y-1 w-full">
        <p className="text-xl font-bold text-slate-900">{title}</p>
        <p className="text-lg  text-slate-800"> {description} </p>
        <p className="text-sm  text-slate-500"> {date} </p>
      </span>
      <span>
        <AiFillDelete className="text-red-600 text-3xl cursor-pointer" />
      </span>
    </div>
  );
};

export default TaskComponent;
