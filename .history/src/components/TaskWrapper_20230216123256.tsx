import React from "react";

const TaskWrapper: React.FC<{ children: React.FC }> = ({ children }) => {
  return <div className="p-2 flex-col space-y-2">{children}</div>;
};

export default TaskWrapper;
