import React from "react";
import { IInput } from "../interfaces";

export const InputComponent: React.FC<IInput> = ({ placeholder, type }) => {
  return (
    <input
      type="text"
      className="px-3 w-full text-slate-700 py-2 outline-none bg-slate-50 border-2 border-slate-400 rounded-sm "
      placeholder="Entrez la description"
    />
  );
};
