import React from "react";
import { IButton } from "../interfaces";

const ButtonComponent: React.FC<IButton> = ({ text, handleSubmit }) => {
  return (
    <button
      className="px-3 py-2 my-2 w-full text-slate-50 text-md bg-slate-900 text-center rounded-sm font-bold uppercase"
      onClick={handleSubmit}
    >
      Ajouter
    </button>
  );
};

export default ButtonComponent;
