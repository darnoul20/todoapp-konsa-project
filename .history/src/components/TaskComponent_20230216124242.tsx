import React from "react";
import { ITaskComponent } from "../interfaces";
import { AiFillDelete } from "react-icons/ai";

const TaskComponent: React.FC<ITaskComponent> = ({
  title,
  description,
  icon,
  date,
}) => {
  return (
    <div className="p-2 border-l-4 border-green-600 flex-row justify-between">
      <span className="flex-col space-y-1">
        <p className="text-xl font-bold text-slate-900"> {title} </p>
        <p className="text-lg font-bold text-slate-800"> {description} </p>
        <p className="text-sm font-bold text-slate-500"> {date} </p>
      </span>
      <AiFillDelete className="text-red-600 text-lg" />
    </div>
  );
};

export default TaskComponent;
