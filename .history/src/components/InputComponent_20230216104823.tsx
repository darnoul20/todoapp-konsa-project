import React, { useState } from "react";
import { IInput } from "../interfaces";

export const InputComponent: React.FC<IInput> = ({ placeholder, type }) => {
  const [change, setChange] = useState();

  return (
    <input
      type={type}
      className="px-3 w-full text-slate-700 py-2 outline-none bg-slate-50 border-2 border-slate-400 rounded-sm "
      placeholder={placeholder}
      onChange={(e) => setChange(e.preventDefault())}
    />
  );
};
