import React, { useState } from "react";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-10 shadow-md flex flex-col rounded-sm bg-slate-50">
        <input
          type="text"
          className="px-3 py-2 outline-none bg-slate-200 border-2 border-slate-300 rounded-sm "
          placeholder="Entrez une tache"
        />
      </div>
    </div>
  );
}

export default App;
