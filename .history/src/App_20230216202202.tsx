import React, { useState } from "react";
import { InputComponent } from "./components/InputComponent";
import { ITask } from "./interfaces";
import ButtonComponent from "./components/ButtonComponent";
import TaskComponent from "./components/TaskComponent";
import TaskWrapper from "./components/TaskWrapper";

function App() {
  const [taskTitle, setTaskTitle] = useState<string>("");
  const [taskDesc, setTaskDesc] = useState<string>("");
  const [taskDate, setTaskDate] = useState<string>("");

  const [newTask, setNewTask] = useState<ITask[]>([]);

  const handleSubmit = () => {
    const values = { title: taskTitle, description: taskDesc, date: taskDate };
    const newTasks = [...newTask, values];
    setNewTask(newTasks);
  };

  console.log(newTask);

  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 lg:w-1/3 w-1/2">
        <div className="flex-col space-y-2 ">
          <InputComponent
            type={"text"}
            placeholder={"Entrez une tache"}
            change={taskTitle}
            setChange={setTaskTitle}
          />
          <InputComponent
            type={"text"}
            placeholder={"Entrez la description"}
            change={taskDesc}
            setChange={setTaskDesc}
          />
          <InputComponent
            type={"date"}
            placeholder={"Choisir la date"}
            change={taskDate}
            setChange={setTaskDate}
          />
          <InputComponent
            type={"toggle"}
            placeholder={"Choisir la date"}
            change={taskDate}
            setChange={setTaskDate}
          />
          <ButtonComponent text="Ajouter" handleSubmit={handleSubmit} />
          <TaskWrapper>
            {newTask.map((task) => (
              <TaskComponent
                title={task.title}
                description={task.description}
                date={task.date}
                isImportant={false}
              />
            ))}
          </TaskWrapper>
        </div>
      </div>
    </div>
  );
}

export default App;
