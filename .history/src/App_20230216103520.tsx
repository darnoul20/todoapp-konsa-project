import React, { useState } from "react";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-2 shadow-md flex flex-col rounded-sm bg-slate-50 w-1/3">
        <div className="flex-row space-x-3 ">
          <input
            type="text"
            className="px-3 w-full py-2 outline-none bg-slate-100 border-2 border-slate-600 rounded-sm "
            placeholder="Entrez une tache"
          />
        </div>
      </div>
    </div>
  );
}

export default App;
