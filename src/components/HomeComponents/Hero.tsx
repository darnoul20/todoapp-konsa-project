import React from "react";
import hero from "../../assets/hero.jpeg";
import ButtonComponent from "../ButtonComponent";
const Hero = () => {
  return (
    <div
      className="w-full flex flex-col lg:flex-row justify-between px-0 lg:px-5 h-[830px] lg:h-[450px] relative"
      style={{ backgroundColor: "#FFFAEE" }}
    >
      <div className="flex flex-col text-left space-y-8 w-full lg:w-1/2 pt-12 px-4 lg:px-8">
        <p className="text-4xl font-bold">
          Des compétences d'aujourd’hui qui ont de l'avenir
        </p>
        <p className="text-md w-11/12">
          Faites un grand pas vers votre nouvelle carrière en décrochant une
          certification reconnue. Notre différence ? Une école 100% en ligne et
          un modèle pédagogique unique qui seront les clés de votre réussite.
        </p>
        <span className="flex space-x-4">
          <ButtonComponent
            addtionalStyling="text-sm px-2 py-2 "
            text={"Demarrer ma candidature"}
            full={true}
          />
          <ButtonComponent
            addtionalStyling="text-sm px-2 py-2 "
            text={"Decouvrir les formations"}
            full={false}
          />
        </span>
      </div>
      <div className="lg:w-1/2 w-full relative lg:top-5">
        <img src={hero} alt="" className="w-full object-cover h-[430px]" />
      </div>
    </div>
  );
};

export default Hero;
