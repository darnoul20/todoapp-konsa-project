import React from "react";
import ButtonComponent from "../ButtonComponent";
import ReactPlayer from "react-player";

const SectionTextVideo = () => {
  return (
    <div className="w-full px-4 lg:px-20 pb-8 space-y-10 lg:space-y-0 flex flex-col lg:flex-row justify-between lg:space-x-10">
      <div className="flex flex-col space-y-8 w-full lg:w-1/2">
        <p className="text-3xl font-bold ">
          Prêt à donner un nouvel élan à votre carrière ?
        </p>
        <p className="text-md ">
          Mettez à jour vos connaissances, développez de nouvelles compétences,
          obtenez une certification reconnue… Quel que soit votre projet de
          carrière, nous sommes là pour vous conseiller et vous accompagner.
          Apprenez un métier qui a de l'avenir avec la référence de l'éducation
          en ligne.
        </p>
        <span className="w-1/2">
          <ButtonComponent
            text={"Demarrer ma candidature"}
            full={true}
            addtionalStyling={"px-2 py-2"}
          />
        </span>
      </div>
      <div className="flex w-full lg:w-1/2 lg:px-10">
        <ReactPlayer
          url="https://www.youtube.com/watch?v=ysz5S6PUM-U"
          height={300}
        />
      </div>
    </div>
  );
};

export default SectionTextVideo;
