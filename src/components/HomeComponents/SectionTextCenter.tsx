import React from "react";
import job_ready from "../../assets/job_ready-7cd674ffd711044da492.png";
import mentoring from "../../assets/mentoring-d3132f53ba0a521499a9.png";
import online_study from "../../assets/online_study-011feaaa33df196e1ae0.png";
const SectionTextCenter = () => {
  return (
    <div className="w-full flex mx-auto flex-col py-24 px-4 lg:px-20">
      <div className="flex flex-col space-y-4 text-center">
        <p className="text-2xl font-bold"> Savoir. Faire. Savoir-faire. </p>
        <p className="text-md text-gray-700">
          Avec OpenClassrooms, découvrez une nouvelle façon d'apprendre : 20% de
          théorie, 80% de pratique.
        </p>
      </div>
      <div className="flex flex-col space-y-10 lg:space-y-0 lg:flex-row justify-between px-8 py-10">
        <div className="flex flex-col space-y-2">
          <div className="flex mx-auto">
            <img src={job_ready} alt="" />
          </div>
          <p className="text-xl font-bold  text-center">
            Savoir. Faire. Savoir-faire
          </p>
          <p className="text-sm text-gray-700  text-center">
            Avec OpenClassrooms, découvrez une nouvelle façon d'apprendre : 20%
            de théorie, 80% de pratique.
          </p>
        </div>
        <div className="flex flex-col space-y-2">
          <div className="flex mx-auto">
            <img src={mentoring} alt="" />
          </div>
          <p className="font-xl font-bold  text-center">
            Savoir. Faire. Savoir-faire.
          </p>
          <p className="font-sm text-gray-700  text-center">
            Avec OpenClassrooms, découvrez une nouvelle façon d'apprendre : 20%
            de théorie, 80% de pratique.
          </p>
        </div>
        <div className="flex flex-col space-y-2 mx-auto">
          <div className="flex mx-auto">
            <img src={online_study} alt="" />
          </div>
          <p className="font-xl font-bold  text-center">
            Savoir. Faire. Savoir-faire.
          </p>
          <p className="font-sm text-gray-700  text-center">
            Avec OpenClassrooms, découvrez une nouvelle façon d'apprendre : 20%
            de théorie, 80% de pratique.
          </p>
        </div>
      </div>
    </div>
  );
};

export default SectionTextCenter;
