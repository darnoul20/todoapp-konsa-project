import React from "react";

const ButtonComponent: React.FC<{
  text: string;
  handleSubmit?: () => {};
  full: boolean;
  addtionalStyling: string;
}> = ({ text, handleSubmit, full, addtionalStyling }) => {
  return (
    <button
      disabled={false}
      className={`w-full ${
        full
          ? "bg-indigo-600 text-white "
          : "border-indigo-600 text-indigo-600 hover:bg-indigo-100 border "
      }  ease delay-100 hover:ease hover:delay-100  text-center rounded-md font-bold ${addtionalStyling}`}
      onClick={handleSubmit}
    >
      {text}
    </button>
  );
};

export default ButtonComponent;
