import React from "react";
import Navbar from "../../layouts/Navbar";
import Hero from "../../components/HomeComponents/Hero";
import SectionTextCenter from "../../components/HomeComponents/SectionTextCenter";
import SectionTextVideo from "../../components/HomeComponents/SectionTextVideo";
import SectionSlider from "../../components/HomeComponents/SectionSlider";
import Footer from "../../layouts/Footer";

const Home = () => {
  return (
    <div className="flex w-full relative flex-col">
      <Navbar />
      <Hero />
      <SectionTextCenter />
      <SectionTextVideo />
      <SectionSlider />
      <Footer />
    </div>
  );
};

export default Home;
